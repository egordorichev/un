#ifndef VARIABLE_HPP_
#define VARIABLE_HPP_

#include "Value.hpp"
#include "Block.hpp"

#include <string>

namespace UN{

	/**
		Presents variable
	*/

	class Variable : public Value{
		public:
			/**
				Initializates class

				@param block: block, where variable is declarated
				@param type: type of value
				@param value: any value
				@overridden
			*/

			Variable(Block *block, Type type, std::string name, boost::any value);

			/**
				Returns block, where variable is declarated
			*/

			Block *getBlock(){ return this->block; };

			/**
				Returns variable type
			*/

			Type getType(){ return this->type; };

			/**
				Returns variable name
			*/

			std::string getName(){ return this->name; };
		private:
			/**
				Variable name
			*/

			std::string name;

			/**
				Block, where variable is declarated
			*/

			Block *block;

			/**
				Variable type
			*/

			Type type;
	};

};

#endif // VARIABLE_HPP_