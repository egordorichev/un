#ifndef VALUE_HPP_
#define VALUE_HPP_

#include <boost/any.hpp>

#include "Type.hpp"

namespace UN{

	/**
		Presents value
	*/

	class Value{
		public:
			/**
				Initializates class

				@param type: type of value
				@param value: any value
			*/

			Value(Type type, boost::any value);

			/**
				Initializates class
			*/

			Value(){ };

			/**
				Sets value

				@param value: any value
			*/

			void setValue(boost::any value){ this->value = value; };

			/**
				Sets type

				@param value: any value
			*/

			void setType(Type type){ this->type = type; };

			/**
				Returns value
			*/

			boost::any getValue(){ return this->value; };

			/**
				Returns type
			*/

			Type getType(){ return this->type; };
		protected:
			/**
				Any value
			*/

			boost::any value;

			/**
				Type
			*/

			Type type;
	};

};

#endif // VALUE_HPP_