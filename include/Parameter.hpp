#ifndef PARAMETER_HPP_
#define PARAMETER_HPP_

#include "Type.hpp"

#include <string>

namespace UN{

	/**
		Presents argument
	*/

	class Parameter{
		public:
			/**
				Initializates class

				@param type: argument type
				@param name: argument name
			*/

			Parameter(Type type, std::string name);

			/**
				Returns argument type
			*/

			Type getType(){ return this->type; };

			/**
				Returns argument name
			*/

			std::string getName(){ return this->name; };
		private:
			/**
				Argument name
			*/

			std::string name;

			/**
				Argument type
			*/

			Type type;
	};

};

#endif // PARAMETER_HPP_