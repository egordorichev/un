#ifndef UN_HPP_
#define UN_HPP_

#include <string>
#include <vector>
#include <fstream>

#include "Tokenizer.hpp"
#include "Class.hpp"
#include "Block.hpp"
#include "Method.hpp"
#include "Variable.hpp"
#include "MethodCall.hpp"

#define LIBRARIES_PATH "libs/"

namespace UN{

	/**
		Main class: interpretator
	*/

	class UN{
		public:
			/**
				Initializates class

				@param filePath: file, which will be parsed
			*/

			UN(std::string filePath);

			/**
				Parses file
			*/

			void parse();

			/**
				Parses file

				@param filePath: file to parse
			*/

			void parse(std::string filePath);

			/**
				Parses line

				@param line: line to parse
			*/

			void parseLine(std::string line);

			/**
				Runs parsed code
			*/

			void run();

			/**
				Enables debug mode
			*/

			void enableDebug(){ this->debug = true; };

			/**
				Parses class

				@param tokenizer: pointer to tokenizer object
				@param line: current line of code
			*/

			Class *parseClass(Tokenizer *tokenizer, std::string line);

			/**
				Parses import

				@param tokenizer: pointer to tokenizer object
				@param line: current line of code
			*/

			Class *parseImport(Tokenizer *tokenizer, std::string line);

			/**
				Returns class with given name or nullptr

				@param name: class name
			*/

			Class *getClass(std::string name);

			/**
				Parses method

				@param tokenizer: pointer to tokenizer object
				@param line: current line of code
			*/

			Method *parseMethod(Tokenizer *tokenizer, std::string line);

			/**
				Parses mthod call

				@param tokenizer: pointer to tokenizer object
				@param line: current line of code
			*/

			MethodCall *parseCall(Tokenizer *tokenizer, std::string line);

			/**
				Parses variable

				@param tokenizer: pointer to tokenizer object
				@param line: current line of code
			*/

			Variable *parseVariable(Tokenizer *tokenizer, std::string line);

			/**
				Parses compiler command

				@param tokenizer: pointer to tokenizer object
				@param line: current line of code
			*/

			MethodCall *parseCompilerCommand(Tokenizer *tokenizer, std::string line);

			std::string extractString(std::string string);
		private:
			/**
				Path to file with code
			*/

			std::string filePath;

			/**
				List of classes
			*/

			std::vector <Class *> classes;

			/**
				Root block
			*/

			Block *rootBlock;

			/**
				Current block
			*/

			Block *currentBlock;

			/**
				Main method
			*/

			Method *main;

			/**
				Enables debug mode
			*/

			bool debug;
	};

};

#endif // UN_HPP_
