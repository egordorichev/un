#ifndef TOKEN_HPP_
#define TOKEN_HPP_

#include <string>

namespace UN{

	enum TokenType{
		TOKEN,
		IDENTIFIER,
		INTEGER_LITERAL,
		STRING_LITERAL,
		EMPTY
	};

	/**
		Converts tokenType to string
	*/

	std::string tokenTypeToString(TokenType type);
	std::string removeQuotes(std::string string);

	/**
		Presents token
	*/

	class Token{
		public:
			/**
				Initializates class

				@param token: string containing token
				@param type: token type
			*/

			Token(std::string token, TokenType type);

			/**
				Initializates class
			*/

			Token(){ };

			/**
				Returns token type
			*/

			TokenType getType(){ return this->type; };

			/**
				Returns token as std::string
			*/

			std::string asString(){ return this->token; };
		private:
			/**
				Token type
			*/

			TokenType type;

			/**
				Token 
			*/

			std::string token;
	};

};

#endif // TOKEN_HPP_