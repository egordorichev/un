#ifndef METHOD_HPP_
#define METHOD_HPP_

#include "Block.hpp"
#include "Value.hpp"
#include "Parameter.hpp"
#include "Type.hpp"

namespace UN{

	/**
		Pressents method
	*/

	class Method : public Block{
		public:
			/**
				Initializates class

				@param superBlock: parent block
				@param name: method name
				@param returnType: type of return value
				@param parameters: which parameters must be passed to this method
				@overridden
			*/

			Method(Block *superBlock, std::string name, Type returnType, std::vector <Parameter> parameters);

			/**
				Returns method name
			*/

			std::string getName(){ return this->name; };

			/**
				Returns block type

				@overridden
			*/

			BlockType getType(){ return METHOD; };

			/**
				Invokes method with value list = {}

				@overridden
			*/

			virtual void run();

			/**
				Invokes method with value list

				@param values: argument values
				@overridden
			*/

			virtual Value *invoke(std::vector <Value> *values);

			/**
				Checks if values are right types and count

				@param values: list of values, given to invoke method
			*/

			void checkValues(std::vector <Value> *values);
		protected:
			/**
				Method name
			*/

			std::string name;

			/**
				Parameters list
			*/

			std::vector <Parameter> parameters;

			/**
				Value, which will be returned in invoke method
			*/

			Value returnValue;

			/**
				Return value type
			*/

			Type returnType;
	};

};

#endif // METHOD_HPP_