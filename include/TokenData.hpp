#ifndef TOKENDATA_HPP_
#define TOKENDATA_HPP_

#include "Token.hpp"

#include <string>
#include <boost/regex.hpp>

namespace UN{

	/**
		Stores data about token
	*/

	class TokenData{
		public:
			/**
				Initializates class

				@param pattern: regular expression, used for token generation
				@param type: token type
			*/

			TokenData(boost::regex pattern, TokenType type);


			/**
				Returns token type
			*/


			TokenType getType(){ return this->type; };

			/**
				Returns regex pattern
			*/

			boost::regex getPattern(){ return this->pattern; };
		private:
			/**
				Token type
			*/

			TokenType type;

			/**
				Regex pattern
			*/

			boost::regex pattern;
	};

};

#endif // TOKEN_HPP_