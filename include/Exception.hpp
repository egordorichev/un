#ifndef EXCEPTION_HPP_
#define EXCEPTION_HPP_

#include <stdexcept>

namespace UN{

	/**
		Exception class, throw on error
	*/

	class Exception : public std::invalid_argument::invalid_argument{
		public:
			/**
				Initializates class

				@param message: message, that will be displayed
				@overridden
			*/

			Exception(std::string message) : invalid_argument(message){ this->message = message; };

			/**
				Returns error message
				@overridden
			*/

			const char *what() const throw(){ return this->message.c_str(); };
		private:
			std::string message;
	};

};

#endif // EXCEPTION_HPP_