#ifndef TYPE_HPP_
#define TYPE_HPP_

#include <string>
#include <boost/any.hpp>

namespace UN{

	/**
		Returns if string contains a number

		@param string: string with number?
	*/

	bool isNumber(std::string string);

	/**
		Returns if string contains a boolean value

		@param string: string with boolean value?
	*/

	bool isBool(std::string string);

	/**
		Returns if string contains a float number

		@param string: string with float number?
	*/

	bool isFloat(std::string string);

	/**
		Returns if string contains a int number

		@param string: string with int number?
	*/

	bool isInt(std::string string);

	/**
		Returns if string contains a string ;)

		@param string: string with string in quotes?
	*/

	bool isString(std::string string);

	/**
		Presents build in types
	*/

	namespace TYPE{

		enum BuildInType{
			VOID,
			INT,
			FLOAT,
			STRING,
			BOOL,

			CLASS
		};


		/**
			Returns default value for this type

			@param type: type :)
		*/

		boost::any defaultFor(TYPE::BuildInType);

	};

	/**
		Presents generetic type class
	*/

	class Type{
		public:
			/**
				Initializates with type

				@param type: which type will be assinged
			*/

			Type(TYPE::BuildInType type);

			/**
				Initializates with type
			*/

			Type(){ };

			/**
				Returns string type or throws exception

				@param string: string to parse
			*/

			static Type match(std::string string);

			static bool isConvertable(Type from, Type to);

			/**
				Returns type
			*/

			TYPE::BuildInType getType(){ return this->type; };

			/**
				Returns type as string
			*/

			std::string asString();

			/**
				Returns new type from string
			*/

			Type operator = (TYPE::BuildInType type);
		private:
			/**
				Stores type
			*/

			TYPE::BuildInType type;

			/**
				Stores class name
			*/

			std::string className;
	};

	/**
		Returns true if types are different
	*/

	bool operator != (Type first, Type second);

	/**
		Returns true if types are the same
	*/

	bool operator == (Type first, Type second);

};

#endif // TYPE_HPP_
