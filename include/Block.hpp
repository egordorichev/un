#ifndef BLOCK_HPP_
#define BLOCK_HPP_

#include <vector>
#include <string>

namespace UN{

	class Variable;

	/**
		Presents block type
	*/

	enum BlockType{
		BLOCK,
		CLASS,
		METHOD,
		METHODCALL,
		IFSTAMENT
	};

	/**
		Presents an exutable block of code
	*/

	class Block{
		public:
			/**
				Initializates this block

				@param superBlock: parent block
			*/

			Block(Block *superBlock);

			/**
				Runs this block with no arguments
			*/

			virtual void run(){ };

			/**
				Adds a sub block to sub blocks list

				@param block: block, which will be added
			*/

			void addSubBlock(Block *block){ this->subBlocks.push_back(block); };

			/**
				Adds a variable to variable list

				@param variable: variable, which will be added
			*/

			void addVariable(Variable *variable){ this->variables.push_back(variable); };

			/**
				Retunrs variable with given name or nullptr

				@param name: name of variable to find
			*/

			Variable *getVariable(std::string name);

			/**
				Returns super block
			*/

			Block *getSuperBlock(){ return this->superBlock; };

			/**
				Returns block type
			*/

			virtual BlockType getType(){ return BLOCK; };

			/**
				Returns sub blocks
			*/

			std::vector <Block *> getSubBlocks(){ return this->subBlocks; };
		protected:
			/**
				Sub blocks list
			*/

			std::vector <Block *> subBlocks;

			/**
				Variables list
			*/

			std::vector <Variable *> variables;

			/**
				Super Block
			*/

			Block *superBlock;
	};

};

#endif // BLOCK_HPP_