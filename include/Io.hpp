#ifndef IO_HPP_
#define IO_HPP_

#include "Method.hpp"

namespace UN{

	class Print : public Method{
		public:
			Print(Block *superBlock);
			void run(){};
			Value *invoke(std::vector <Value> *values);
		private:
	};

};

#endif // IO_HPP_