#ifndef METHODCALL_HPP_
#define METHODCALL_HPP_

#include "Block.hpp"
#include "Value.hpp"

namespace UN{

	class Method;

	/**
		Pressents method call
	*/

	class MethodCall : public Block{
		public:
			/**
				Initializates class

				@param superBlock: parent block
				@param method: pointer to method, which will be called
				@param values: which parameters must be passed to this method
				@overridden
			*/

			MethodCall(Block *superBlock, Method *method, std::vector <Value> values);

			/**
				Returns block type

				@overridden
			*/

			BlockType getType(){ return METHODCALL; };

			/**
				Calls method (by pointer) with values, given in constructor
			*/

			void run();

			/**
				Calls method (by pointer) with given values
			*/

			Value *invoke(std::vector <Value> *values);
		private:
			/**
				Values, which will be passed to method::invoke()
			*/

			std::vector <Value> values;

			/**
				Method pointer, which will be invoked
			*/

			Method *method;
	};

};

#endif // METHODCALL_HPP_