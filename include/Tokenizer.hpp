#ifndef TOKENIZER_HPP_
#define TOKENIZER_HPP_

#include <string>
#include <boost/regex.hpp>

#include "Token.hpp"
#include "TokenData.hpp"

namespace UN{

	/**
		Tokenizates a string
	*/

	class Tokenizer{
		public:
			/**
				Initializates class

				@param code: line, which will be tokenizated
			*/

			Tokenizer(std::string code);

			/**
				Returns next token in line and erases this token from line
			*/

			Token nextToken();

			/**
				Returns next token in line
			*/

			Token peekToken();

			/**
				Returns if it has next token in line
			*/

			bool hasNextToken(){ return !this->code.empty(); };
		private:
			/**
				Line, which will be tokenizated
			*/

			std::string code;

			/**
				List of token datas
			*/

			std::vector <TokenData> tokenDatas;
	};

};

#endif // TOKENIZER_HPP_