#ifndef CLASS_HPP_
#define CLASS_HPP_

#include "Block.hpp"

namespace UN{

	/**
		Presents class block. Extends from block class
	*/

	class Class : public Block{
		public:
			/**
				Initializates class

				@param superBlock: parent block
				@param name: name of class
			*/

			Class(Block *superBlock, std::string name);

			/**
				Returns block type

				@overridden
			*/

			BlockType getType(){ return CLASS; };

			/**
				Finds main method and runs it

				@overridden
			*/

			void run();

			/**
				Returns class name
			*/

			std::string getName(){ return this->name; };
		private:
			/**
				Class name
			*/

			std::string name;
	};

};

#endif // CLASS_HPP_