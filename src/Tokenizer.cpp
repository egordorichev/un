#include "Tokenizer.hpp"

#include <fstream>
#include <iostream>
#include <boost/regex.hpp>

namespace UN{

	std::string tokens[]{
		"^(\\))", "^(\\{)", "^(\\})", "^(\\:)", "^(-)", "^(\\+)", "^(=)", "^(\\()",
		 "^(\\@)", "^(\')", "^(\\*)", "^(\\&)", "^(\\^)", "^(\\#)", "^(\\!)",
		"^(\\.)", "^(\\,)", "^(\")", "^(\\;)", "^(\\[)", "^(\\[)", "^(\\~)", "^(\\|)"
	};

	Tokenizer::Tokenizer(std::string code){
		this->code = code;
		this->tokenDatas.push_back(TokenData(boost::regex("^([a-zA-Z][a-zA-Z0-9]*)"), IDENTIFIER));
		this->tokenDatas.push_back(TokenData(boost::regex("^((-)?[0-9]+)"), INTEGER_LITERAL));
		this->tokenDatas.push_back(TokenData(boost::regex("^(\".*\")"), STRING_LITERAL));

		for(std::string token : tokens){
			this->tokenDatas.push_back(TokenData(boost::regex(token), TOKEN));
		}
	}

	Token Tokenizer::peekToken(){
		this->code.erase(this->code.begin(), std::find_if(this->code.begin(), this->code.end(), std::not1(std::ptr_fun <int, int> (std::isspace)))); // Delete white spaces

		if(!this->hasNextToken()){
			return Token("", EMPTY);
		}

		for(TokenData tokenData : this->tokenDatas){
			boost::smatch matchs;

			if(boost::regex_search(this->code, matchs, tokenData.getPattern())){
				std::string token = matchs.str();

				return Token(token, tokenData.getType());
			}
		}

		std::cout << "Unrecognazed token\n";
	}

	Token Tokenizer::nextToken(){
		this->code.erase(this->code.begin(), std::find_if(this->code.begin(), this->code.end(), std::not1(std::ptr_fun <int, int> (std::isspace)))); // Delete white spaces

		if(!this->hasNextToken()){
			return Token("", EMPTY);
		}

		for(TokenData tokenData : this->tokenDatas){
			boost::smatch matchs;

			if(boost::regex_search(this->code, matchs, tokenData.getPattern())){
				std::string token = matchs.str();
				this->code = this->code.substr(token.size());

				return Token(token, tokenData.getType());
			}
		}

		std::cout << "Unrecognazed token\n";
	}

};
