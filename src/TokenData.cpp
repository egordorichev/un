#include "TokenData.hpp"

namespace UN{

	TokenData::TokenData(boost::regex pattern, TokenType type){
		this->pattern = pattern;
		this->type = type;
	}

};