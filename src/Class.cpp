#include "Class.hpp"
#include "Method.hpp"

#include <iostream>

namespace UN{
	
	Class::Class(Block *superBlock, std::string name) : Block(superBlock){
		this->name = name;
	}

	void Class::run(){
		for(Block block : this->getSubBlocks()){
			Method *method;

			if(method = dynamic_cast <Method *> (&block)){
				if(method->getName() == "main"){
					method->run();
				}
			}
		}
	}
	
};