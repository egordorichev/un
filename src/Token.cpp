#include "Token.hpp"

namespace UN{

	std::string tokenTypeToString(TokenType type){
		switch(type){
			case TOKEN: return "token";
			case IDENTIFIER: return "idenfitifier";
			case INTEGER_LITERAL: return "integer literal";
			case STRING_LITERAL: return "string literal";
			case EMPTY: return "empty";
		}
	}

	std::string removeQuotes(std::string string){
		if(string[0] == '\"'){
			string.erase(0, 1); // Remove "
		}

		if(string[string.size() - 1] == '\"'){
			string.pop_back(); // Remove "
		}

		return string;
	}

	Token::Token(std::string token, TokenType type){
		this->token = token;
		this->type = type;
	}

};