#include "Method.hpp"
#include "Variable.hpp"
#include "Exception.hpp"

#include <iostream>

namespace UN{

	Method::Method(Block *superBlock, std::string name, Type returnType, std::vector <Parameter> parameters) : Block(superBlock){
		this->name = name;
		this->returnType = returnType;
		this->parameters = parameters;

		for(Parameter parameter : parameters){
			this->addVariable(new Variable(this, parameter.getType(), parameter.getName(), TYPE::defaultFor(parameter.getType().getType())));
		}
	}

	void Method::run(){
		invoke(new std::vector <Value> ());
	}

	void Method::checkValues(std::vector <Value> *values){
		if(values->size() != this->parameters.size()){
			throw Exception("Number of values doesn't match number of parameters in method \'" + this->name + "\'. Must be " + std::to_string(parameters.size()) + ", got " + std::to_string(values->size()));
		}

		for(int i = 0; i < values->size() && parameters.size(); i++){
			Parameter parameter = parameters[i];
			Value value = (*values)[i];

			if(parameter.getType() != value.getType()){
				throw Exception("Parameter " + parameter.getName() + " should be " + parameter.getType().asString() + "; Got " + value.getType().asString());
			}

			this->getVariable(parameter.getName())->setValue(value.getValue());
		}

		if(this->returnValue.getValue().empty() && this->returnType != TYPE::VOID){
			throw Exception("Expected return value, got none.");
		}
	}

	Value *Method::invoke(std::vector <Value> *values){
		for(Block *block : this->getSubBlocks()){
			block->run();

			if(!this->returnValue.getValue().empty()){ // TODO
				break;
			}
		}

		Value localReturnValue = this->returnValue;
		this->returnValue = Value();

		return &localReturnValue;
	}

};
