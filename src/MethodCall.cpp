#include "MethodCall.hpp"
#include "Method.hpp"

#include <iostream>

namespace UN{

	MethodCall::MethodCall(Block *superBlock, Method *method, std::vector <Value> values) : Block(superBlock){
		this->values = values;
		this->method = method;
	}

	void MethodCall::run(){
		this->invoke(&this->values);
	}

	Value *MethodCall::invoke(std::vector <Value> *values){
		if(this->method != nullptr){
			for(int i = 0; i < this->values.size(); i++){
				std::cout << boost::any_cast <std::string> (this->values[i].getValue());
			}

			return this->method->invoke(values);
		}
	}

};