#include "Io.hpp"

#include <iostream>
#include <cstdio>

namespace UN{

	Print::Print(Block *superBlock) : Method(superBlock, "print", Type(TYPE::VOID), { Parameter(Type(TYPE::STRING), "string") }){

	}

	Value *Print::invoke(std::vector <Value> *values){
		this->checkValues(values);

		for(int i = 0; i < values->size(); i++){
			std::cout << boost::any_cast <std::string> ((*values)[i].getValue()) << "\n";
		}

		return new Value(Type(TYPE::VOID), boost::any());
	}

};
