#include <iostream>
#include <string>
#include <readline/readline.h>
#include <readline/history.h>

#include "UN.hpp"

void printHelp(){
	std::cout << "un [file] [options]\n\t-c or --check\tOnly check code, don't run it\n\t-d or --debud\tEnable debug mode\n";
}

int main(int argc, char const *argv[]){
	std::string filePath;

	bool runFile = true;
	bool debug = false;

	for(int i = 1; i < argc; i++){
		if(std::string(argv[i]) == "--help" || std::string(argv[i]) == "-h"){
			printHelp();

			exit(0);
		} else if(std::string(argv[i]) == "--check" || std::string(argv[i]) == "-c"){
			runFile = false;
		} else if(std::string(argv[i]) == "--debug" || std::string(argv[i]) == "-d"){
			debug = true;
		} else {
			filePath = argv[i];
		}
	}

	UN::UN un(filePath);

	if(debug){
		un.enableDebug();
	}

	if(filePath.empty()){
		int state = 1;

		do {
			char *line = readline(">>> ");

			if(!line){
				continue;
			}

			add_history(line);

			if(std::string(line) == "help"){
				printHelp();
			} else if(std::string(line) == "exit"){
				exit(0);
			} else if(std::string(line) == "run"){
				un.run();
			} else {
				un.parseLine(line);
			}

			delete line;
		} while(state == 1);
	} else {
		un.parse();

		if(runFile){
			un.run();
		}
	}

	return 0;
}