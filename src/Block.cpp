#include "Block.hpp"
#include "Variable.hpp"

namespace UN{

	Block::Block(Block *superBlock){
		this->superBlock = superBlock;
	}

	Variable *Block::getVariable(std::string name){
		for(auto variable : this->variables){
			if(variable->getName() == name){
				return variable;
			}
		}

		if(this->superBlock != nullptr){
			return this->superBlock->getVariable(name);
		}

		return nullptr;
	}

};