#include "UN.hpp"
#include "Exception.hpp"
#include "Type.hpp"
#include "Io.hpp"

#include <iostream>
#include <cstdlib>
#include <string>

#include <boost/lexical_cast.hpp>

namespace UN{

	UN::UN(std::string filePath){
		this->filePath = filePath;
		this->main = nullptr;
		this->debug = false;
	}

	void UN::parse(){
		this->parse(this->filePath);
	}

	void UN::parse(std::string filePath){
		this->rootBlock = new Block(nullptr);
		this->currentBlock = rootBlock;

		std::fstream file(filePath, std::ios::in | std::ios::binary);

		if(!file.good()){
			throw Exception("Couldn't find needed file\n");
		}

		int lineCount = 0;

		do {
			std::string line;

			std::getline(file, line);
			line.erase(line.begin(), std::find_if(line.begin(), line.end(), std::not1(std::ptr_fun <int, int> (std::isspace)))); // Delete white spaces

			lineCount++;

			try {
				this->parseLine(line);
			} catch (Exception &exception){
				std::cout << filePath << " line " << lineCount << ": " << exception.what() << "\n";

				exit(1);
			}
		} while(file.good());

	}

	void UN::parseLine(std::string line){
		if(line == ""){
			return;
		}

		Tokenizer *tokenizer = new Tokenizer(line);
		std::string nextToken = tokenizer->peekToken().asString();

		if(nextToken == "cl"){
			Class *cl = this->parseClass(tokenizer, line);
			this->currentBlock->addSubBlock((Block *)cl);
			this->currentBlock = cl;

			if(this->debug){
				std::cout << "Found class with name " + cl->getName()  << "\n";
			}

			this->classes.push_back(cl);
		} else if(nextToken == "fn"){
			Method *method = this->parseMethod(tokenizer, line);

			if(this->currentBlock->getType() == CLASS){ // Hm.. it's method in a class..
				this->currentBlock->addSubBlock(method);
				this->currentBlock = method;

				if(method->getName() == "main"){
					this->main = method;

					if(this->debug){
						std::cout << "Found main method"  << "\n";
					}
				} else {
					if(this->debug){
						std::cout << "Found method with name " + method->getName() << "\n";
					}
				}
			} else {
				throw Exception("Couldn't cretate method " + method->getName() + " outside of a class");
			}
		} else if(nextToken == "import"){
			Class *cl = this->parseImport(tokenizer, line);

			if(cl != nullptr){
				this->classes.push_back(cl);
			}
		} else if(nextToken == "type"){
			Variable *variable = this->parseVariable(tokenizer, line);

			if(this->currentBlock == nullptr){
				throw Exception("this->currentBlock == nullptr");
			}

			if(this->currentBlock->getType() != METHOD && this->currentBlock->getType() != CLASS){
					throw Exception("Couldn't cretate variable outside of a class");
			} else {
				if(this->debug){
					std::cout << "Found variable with name " << variable->getName() << "\n";
				}

				this->currentBlock->addVariable(variable);
			}
		} else if(nextToken == "}"){
			if(this->currentBlock->getSuperBlock() == nullptr){
				throw Exception("Unexpected token \'}\': " + line);
			} else {
				this->currentBlock = currentBlock->getSuperBlock();
			}
		} else if(this->getClass(nextToken) != nullptr){

			MethodCall *methodCall = this->parseCall(tokenizer, line);

			if(this->currentBlock->getType() != METHOD){
				throw Exception("Couldn't call method outside of method");
			} else {
				if(this->debug){
					std::cout << "Found method call\n";
				}

				this->currentBlock->addSubBlock(methodCall);
			}
		} else if(nextToken == "~"){
			this->currentBlock->addSubBlock(this->parseCompilerCommand(tokenizer, line));
		} else {
			throw Exception("Invalid token: " + nextToken);
		}
	}

	void UN::run(){
		if(this->main != nullptr){
			if(this->debug){
				std::cout << "Running main method...\n";
			}

			this->main->run();
		} else {
			throw Exception("Main method not found");
		}
	}

	Class *UN::parseClass(Tokenizer *tokenizer, std::string line){
		tokenizer->nextToken(); // Skip class
		Token token = tokenizer->nextToken();

		if(token.getType() == IDENTIFIER){
			std::string className = token.asString();
			token = tokenizer->nextToken();

			if(token.asString() == "{"){
				return new Class(this->currentBlock, className);
			} else {
				throw Exception("Unexpected token: \'" + token.asString() + "\'. Expecting \'{\' token");
			}
		} else {
			throw Exception("Unexpected token: \'" + token.asString() + "\'. Expecting class name");
		}
	}

	Class *UN::parseImport(Tokenizer *tokenizer, std::string line){
		tokenizer->nextToken(); // Skip import
		Token token = tokenizer->nextToken();

		if(token.getType() != IDENTIFIER){
			throw Exception("Unexpected token: \'" + token.asString() + "\'. Expecting file to import");
		} else {
			this->parse(LIBRARIES_PATH + token.asString() + ".un");
		}

		token = tokenizer->nextToken();

		if(token.asString() != ";"){
			throw Exception("Missing semicolon");
		}
	}

	Class *UN::getClass(std::string name){
		if(name == "this") {
			if(this->currentBlock->getType() == CLASS) {
				return static_cast <Class *> (this->currentBlock);
			} else if(this->currentBlock->getType() == METHOD) {
				return static_cast <Class *> (this->currentBlock->getSuperBlock());
			}
		}		

		for(Class *cl : this->classes){
			if(cl->getName() == name){
				return cl;
			}
		}

		return nullptr;
	}

	Method *UN::parseMethod(Tokenizer *tokenizer, std::string line){
		tokenizer->nextToken(); // Skip fn
		Token token = tokenizer->nextToken();

		if(token.asString() != "type"){
			throw Exception("Missing method type: " + line);
		} else {
			// TODO: set return type;

			token = tokenizer->nextToken();

			if(token.asString() != ":"){
				throw Exception("Missing \':\': " + line);
			} else {
				token = tokenizer->nextToken();

				if(token.getType() != IDENTIFIER){
					throw Exception("Missing type token: " + line);
				} else {
					std::string returnType = token.asString();
					token = tokenizer->nextToken();

					if(token.getType() != IDENTIFIER){
						throw Exception("Missing method name: " + line);
					} else {
						std::string methodName = token.asString();
						token = tokenizer->nextToken();

						if(token.asString() == "("){
							token = tokenizer->nextToken();

							std::vector <Parameter> parameters;

							while(token.asString() != ")"){
								if(token.asString() != "type"){
									throw Exception("\'type\' missed, TODO");
								} else {
									token = tokenizer->nextToken();

									if(token.asString() != ":"){
										throw Exception("\':\' missed, TODO");
									} else {
										token = tokenizer->nextToken();

										Type parameterType = Type::match(token.asString());

										token = tokenizer->nextToken();

										std::string parameterName = token.asString();

										parameters.push_back(Parameter(parameterType, parameterName));
									}
								}

								token = tokenizer->nextToken();
							}

							return new Method(this->currentBlock, methodName, Type::match(returnType), parameters);
						} else {
							throw Exception("Unexpected token: \'" + token.asString() + "\'. Expecting \'(\' token");
						}
					}
				}
			}
		}
	}

	MethodCall *UN::parseCall(Tokenizer *tokenizer, std::string line){
		std::string className = tokenizer->nextToken().asString();

		if(this->getClass(className) == nullptr){
			throw Exception("Couldn't find class with name: " + className);
		}

		Token token = tokenizer->nextToken();

		if(token.asString() != ":"){
			throw Exception("Unexpected token: \'" + token.asString() + "\'. Expecting \':\' token");
		} else {
			token = tokenizer->nextToken();

			if(token.getType() != IDENTIFIER){
				throw Exception("Unexpected token: \'" + token.asString() + "\'. Expecting method name token");
			} else {
				std::string methodName = token.asString();
				token = tokenizer->nextToken();
				bool methodFound = false;
				Method *method;

				for(Block *block : this->getClass(className)->getSubBlocks()){
					if(block->getType() == METHOD){
						if(((Method *)block)->getName() == methodName){
							method = (Method *)block;
							methodFound = true;
						}
					}
				}

				if(!methodFound){
					throw Exception("Couldn't find method with name " + methodName + " in class " + className);
				}

				if(token.asString() != "("){
					throw Exception("Unexpected token: \'" + token.asString() + "\'. Expecting \'(\' token");
				} else {
					token = tokenizer->nextToken();

					std::vector <Value> values;
					std::string stringValue;

					while(token.asString() != ")"){
						boost::any value;
						Type type;

						Variable *variable = this->currentBlock->getVariable(token.asString());

						if(variable != nullptr){
							type = variable->getType();
							value = variable->getValue();
						} else {
							type = Type::match(token.asString());

							std::string string;

							switch(type.getType()){
								case TYPE::INT: value = std::stoi(token.asString()); break;
								case TYPE::FLOAT: value = std::stof(token.asString()); break;
								case TYPE::BOOL: value = boost::lexical_cast <bool> (token.asString()); break;
								case TYPE::STRING:
									string = token.asString();
									string.erase(remove(string.begin(), string.end(), '\"'), string.end());
									value = string;
								break;
								case TYPE::CLASS: break; // TODO
							}

						}

						token = tokenizer->nextToken();
						values.push_back(Value(type, value));
					}

					return new MethodCall(this->currentBlock, method, values);
				}
			}
		}
	}

	Variable *UN::parseVariable(Tokenizer *tokenizer, std::string line){
		tokenizer->nextToken(); // Skip type
		Token token = tokenizer->nextToken();

		if(token.asString() != ":"){
			throw Exception("Unexpected token: \'" + token.asString() + "\'. Expecting \':\' token");
		} else {
			token = tokenizer->nextToken();

			if(token.getType() != IDENTIFIER){
				throw Exception("Unexpected token: \'" + token.asString() + "\'. Expecting \':\' token");
			} else {
				Type type = Type::match(token.asString());
				token = tokenizer->nextToken();


				if(token.getType() != IDENTIFIER){
					throw Exception("Unexpected token: \'" + token.asString() + "\'. Expecting variable name");
				} else {
					std::string name = token.asString();
					boost::any value;
					token = tokenizer->nextToken();

					if(token.asString() == ";"){
						return new Variable(this->currentBlock, type.getType(), name, value);
					} else if(token.asString() != "="){
						throw Exception("Unexpected token: \'" + token.asString() + "\'. Expecting \';\' or \'=\'");
					} else {
						token = tokenizer->nextToken();

						if(isNumber(token.asString())){
							if(isFloat(token.asString())){
								if(type.getType() != TYPE::BOOL &&
									type.getType() != TYPE::INT && type.getType() != TYPE::FLOAT){

									throw Exception("Couldn convert float to " + type.asString());
								} else {
									value = std::stof(token.asString());
								}
							} else if(isInt(token.asString())){
								if(type.getType() != TYPE::BOOL &&
									type.getType() != TYPE::INT && type.getType() != TYPE::FLOAT){

									throw Exception("Couldn convert int to " + type.asString());
								} else {
									value = std::stoi(token.asString());
								}
							} else {
								throw Exception("Unknown number type " + token.asString());
							}
						} else if(isString(token.asString())){
							if(type.getType() != TYPE::STRING){
								throw Exception("Couldn convert string to " + type.asString());
							} else {
								value = removeQuotes(token.asString());
							}
						} else if(isBool(token.asString())){
							if(type.getType() != TYPE::BOOL &&
								type.getType() != TYPE::INT && type.getType() != TYPE::FLOAT){

								throw Exception("Couldn convert bool to " + type.asString());
							} else {
								if(token.asString() == "true" || token.asString() == "1"){
									value = true;
								} else {
									value = false;
								}
							}
						} else if(token.getType() == IDENTIFIER){
							Variable *variable = this->currentBlock->getVariable(name);

							if(variable == nullptr){
								throw Exception("Couldn't find variable " + name);
							} else {
								if(type != variable->getType()){
									throw Exception("Couldn't convert " + type.asString() + " to " + variable->getType().asString());
								} else {
									value = std::stoi(removeQuotes(token.asString()));
								}
							}
						} else {
							throw Exception("Unexpected token: \'" + token.asString() + "\'. Expecting variable value");
						}

						return new Variable(this->currentBlock, type, name, value);
					}
				}
			}
		}
	}

	MethodCall *UN::parseCompilerCommand(Tokenizer *tokenizer, std::string line){
		tokenizer->nextToken(); // Skip ~
		Token token = tokenizer->nextToken();

		if(token.getType() != IDENTIFIER){
			throw Exception("Unexpected token \'" + token.asString() + "\'. Expecting method name");
		} else {
			std::string methodName = token.asString();
			token = tokenizer->nextToken();

			if(methodName == "print"){
				Variable *variable = this->currentBlock->getVariable(token.asString());
				std::string value;

				if(variable != nullptr){
					value = boost::any_cast <std::string> (variable->getValue());

					std::cout << value;
				} else {
					throw Exception("Variable " + token.asString() + " is not defined");
				}

				return new MethodCall(this->currentBlock, (Method *)new Print(this->currentBlock), { Value(Type(TYPE::STRING), value) });
			} else {
				throw Exception("Unknown method \'" + methodName + "\'");
			}

			if(token.asString() != "~"){
				throw Exception("Missing \'~\' symbol");
			}
		}
	}

}
