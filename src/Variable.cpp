#include "Variable.hpp"

namespace UN{

	Variable::Variable(Block *block, Type type, std::string name, boost::any value){
		this->block = block;
		this->type = type;
		this->name = name;
		this->value = value;
	}

};