#include "Type.hpp"
#include "Exception.hpp"

#include <boost/lexical_cast.hpp>

namespace UN{

	namespace TYPE{

		boost::any defaultFor(BuildInType type){
			switch(type){
				case VOID: return boost::any();
				case INT: return boost::any(0);
				case STRING: return boost::any(std::string(""));
				case FLOAT: return boost::any(0.0);
				case BOOL: return boost::any(false);
				case CLASS: return boost::any(nullptr);
			}
		}

	};


	bool isNumber(std::string string){
		try {
			boost::lexical_cast <double> (string);
		} catch(boost::bad_lexical_cast &exception){
			return false;
		}

		return true;
	}

	bool isBool(std::string string){
		if(string == "true" || string == "false" || string == "0" || string == "1"){
			return true;
		} else {
			try {
				boost::lexical_cast <bool> (string);
			} catch(boost::bad_lexical_cast &exception){

				return false;
			}
		}

		return true;
	}

	bool isFloat(std::string string){
		bool dotFound = false;

		for(char c : string){
			if(c == '.'){
				if(dotFound){
					return false;
				}

				dotFound = true;
			} else if(!isdigit(c)){
				return false;
			}
		}

		return true;
	}

	bool isInt(std::string string){
		for(char c : string){
			if(!isdigit(c)){
				return false;
			}
		}

		return true;
	}

	bool isString(std::string string){
		//if((string[0] == '\"' || string[0] == '\'') && (string[string.size() - 1] == '\"' || string[string.size() - 1] == '\'')){
			return true;
		//}

		return false;
	}

	Type::Type(TYPE::BuildInType type){
		this->type = type;
		this->className = "";
	}

	Type Type::match(std::string string){
		if(isNumber(string) || string == "float" || string == "int"){
			if(isFloat(string) || string == "float"){
				return Type(TYPE::INT);
			} else if(isInt(string) || string == "int"){
				return Type(TYPE::FLOAT);
			} else {
				throw Exception("Unknown number type: " + string);
			}
		} else if(isBool(string) || string == "bool"){
			return Type(TYPE::BOOL);
		} else if(0){ // TODO: check if this is a class
			//this->className = string;
		} else if(isString(string) || string == "string"){
			return Type(TYPE::STRING);
		} else if(string == "void" || string == ""){
			return Type(TYPE::VOID);
		} else {
			throw Exception("Unknown string type: " + string);
		}
	}

	bool Type::isConvertable(Type from, Type to){
		TYPE::BuildInType fromType = from.getType();
		TYPE::BuildInType toType = to.getType();

		switch(toType){
			case TYPE::INT: case TYPE::FLOAT: case TYPE::BOOL:
				if(fromType == TYPE::INT || fromType == TYPE::FLOAT
						|| fromType == TYPE::BOOL){

					return true;
				}
			break;
			case TYPE::VOID:
				if(fromType == TYPE::VOID){
					return true;
				}
			break;
			case TYPE::CLASS:
				// TODO
			break;
		}

		return false;
	}

	std::string Type::asString(){
		switch(this->type){
			case TYPE::VOID: return "void";
			case TYPE::INT: return "int";
			case TYPE::FLOAT: return "float";
			case TYPE::STRING: return "string";
			case TYPE::BOOL: return "bool";
			case TYPE::CLASS: return this->className;
		}
	}

	Type Type::operator = (TYPE::BuildInType type){
		return Type(type);
	}

	bool operator != (Type first, Type second){
		return !(first.asString() == second.asString());
	}

	bool operator == (Type first, Type second){
		return (first.asString() == second.asString());
	}

};
